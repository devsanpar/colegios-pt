<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::apiResource('colegios', 'API\ColegiosController');
Route::apiResource('profesores', 'API\ProfesoresController');
Route::apiResource('alumnos', 'API\AlumnosController');
Route::apiResource('usuarios', 'API\UsuariosController');
Route::get('colegioProfesores/{colegioId}', 'API\ColegiosController@getProfesoresByColegio');
Route::get('colegioAlumnos/{colegioId}', 'API\ColegiosController@getAlumnosByColegio');
Route::post('usuarios/{usuarioId}', 'API\UsuariosController@update');
Route::post('colegios/{colegioId}', 'API\ColegiosController@update');
Route::post('profesores/{profesorId}', 'API\ProfesoresController@update');
Route::post('alumnos/{alumnoId}', 'API\AlumnosController@update');