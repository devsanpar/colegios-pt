var directorio = "http://localhost:8080/colegios/public/";
var app = angular.module("pruebaTecnica", []);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
});
//CONTROLADOR PARA USUARIOS
app.controller("usuariosController", function ($scope, $http) {
    $scope.usuarios = [];
    $scope.nuevoUsuario = {};
    //trae la lista de usuarios
    $http.get(directorio + "api/usuarios")
        .then(function (response) {
                console.log(response);
                $scope.usuarios = response.data;
            },
            function (error) {
                console.log(error)
            });
    //trae datos de  un usuarios
    $scope.leerUsuario = function (id) {
        $http.get(directorio + "api/usuarios/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.nuevoUsuario.txtNombre = response.data[0].nombre;
                    $scope.nuevoUsuario.txtApellido = response.data[0].apellido;
                    $scope.nuevoUsuario.txtUsuario = response.data[0].usuario;
                    $scope.nuevoUsuario.pssClave = response.data[0].clave;
                },
                function (error) {
                    console.log(error)
                });
    }
    //se registra un nuevo usuario
    $scope.agregarUsuario = function () {
        var request = $http({
            method: "post",
            url: directorio + "api/usuarios",
            data: $scope.nuevoUsuario,
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "listaUsuarios";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se actualiza un usuario
    $scope.actualizarUsuario = function (id) {
        var request = $http({
            method: "post",
            url: directorio + "api/usuarios/" + id,
            data: {
                txtNombre: $scope.nuevoUsuario.txtNombre,
                txtApellido: $scope.nuevoUsuario.txtApellido,
                txtUsuario: $scope.nuevoUsuario.txtUsuario,
                pssClave: $scope.nuevoUsuario.pssClave,
                _token: $scope.nuevoUsuario.token,
                hddUsuarioId: $scope.nuevoUsuario.hddUsuarioId
            },
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "../listaUsuarios";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se elimina un usuario
    $scope.eliminarUsuario = function (id) {
        var url = directorio + "api/usuarios/" + id;
        $http.delete(url)
            .then(function (response) {
                    console.log(response);
                    if (response.data.mensaje == "OK") {
                        $('.bs-example-modal_eliminarEx').modal('show');
                        setTimeout(function () {
                            window.location.href = "listaUsuarios";
                        }, 3000);
                    } else {
                        $('.bs-example-modal_error').modal('show');
                    }
                },
                function (error) {
                    $('.bs-example-modal_error').modal('show');
                });
    }
});

//CONTROLADOR PARA COLEGIOS
app.controller("colegiosController", function ($scope, $http) {
    $scope.colegios = [];
    $scope.nuevoColegio = {};
    $scope.profesores = [];
    $scope.alumnos = [];
    //trae la lista de colegios
    $http.get(directorio + "api/colegios")
        .then(function (response) {
                console.log(response);
                $scope.colegios = response.data;
            },
            function (error) {
                console.log(error)
            });
    //traer la lista de profesores por colegio
    $scope.leerProfesoresColegio = function (id) {
        $http.get(directorio + "api/colegioProfesores/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.profesores = response.data;
                },
                function (error) {
                    console.log(error)
                });
    };
    //traer la lista de alumnos por colegio
    $scope.leerAlumnosColegio = function (id) {
        $http.get(directorio + "api/colegioAlumnos/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.alumnos = response.data;
                },
                function (error) {
                    console.log(error)
                });
    };
    //trae datos de  un colegio
    $scope.leerColegio = function (id) {
        $http.get(directorio + "api/colegios/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.nuevoColegio.txtNombre = response.data[0].nombre;
                    $scope.nuevoColegio.txtDireccion = response.data[0].direccion;
                    $scope.nuevoColegio.txtPersona = response.data[0].personaContacto;
                    $scope.nuevoColegio.txtTelefono = response.data[0].telefono;
                    $scope.nuevoColegio.txtCorreo = response.data[0].correo;
                    $scope.nuevoColegio.txtWeb = response.data[0].web;
                },
                function (error) {
                    console.log(error)
                });
    }
    //se registra un nuevo usuario
    $scope.agregarColegio = function () {
        var request = $http({
            method: "post",
            url: directorio + "api/colegios",
            data: $scope.nuevoColegio,
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "listaColegios";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se actualiza un colegio
    $scope.actualizarColegio = function (id) {
        var request = $http({
            method: "post",
            url: directorio + "api/colegios/" + id,
            data: {
                txtNombre: $scope.nuevoColegio.txtNombre,
                txtDireccion: $scope.nuevoColegio.txtDireccion,
                txtPersona: $scope.nuevoColegio.txtPersona,
                txtCorreo: $scope.nuevoColegio.txtCorreo,
                _token: $scope.nuevoColegio.token,
                hddcolegioId: $scope.nuevoColegio.hddcolegioId,
                txtWeb: $scope.nuevoColegio.txtWeb,
                txtTelefono: $scope.nuevoColegio.txtTelefono
            },
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "../listaColegios";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se elimina un colegio
    $scope.eliminarColegio = function (id) {
        var url = directorio + "api/colegios/" + id;
        $http.delete(url)
            .then(function (response) {
                    console.log(response);
                    if (response.data.mensaje == "OK") {
                        $('.bs-example-modal_eliminarEx').modal('show');
                        setTimeout(function () {
                            window.location.href = "listaColegios";
                        }, 3000);
                    } else {
                        $('.bs-example-modal_error').modal('show');
                    }
                },
                function (error) {
                    $('.bs-example-modal_error').modal('show');
                });
    }
});

//CONTROLADOR PARA Profesores
app.controller("profesoresController", function ($scope, $http) {
    $scope.profesores = [];
    $scope.nuevoProfesor = {};
    $scope.colegios = [];
    //trae la lista de colegios
    $http.get(directorio + "api/colegios")
        .then(function (response) {
                console.log(response);
                $scope.colegios = response.data;
            },
            function (error) {
                console.log(error)
            });
    //trae la lista de profesores
    $http.get(directorio + "api/profesores")
        .then(function (response) {
                console.log(response);
                $scope.profesores = response.data;
            },
            function (error) {
                console.log(error)
            });
    //trae datos de  un profesor
    $scope.leerProfesor = function (id) {
        $http.get(directorio + "api/profesores/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.nuevoProfesor.txtNombre = response.data[0].nombre;
                    $scope.nuevoProfesor.txtApellido = response.data[0].apellido;
                    $scope.nuevoProfesor.txtDni = response.data[0].dni;
                    $scope.nuevoProfesor.txtTelefono = response.data[0].telefono;
                    $scope.nuevoProfesor.txtCorreo = response.data[0].correo;
                    $scope.nuevoProfesor.selectColegio = response.data[0].Colegio;
                    $scope.nuevoProfesor.txtCatedra = response.data[0].catedra;
                },
                function (error) {
                    console.log(error)
                });
    }
    //se registra un nuevo profesor
    $scope.agregarProfesor = function () {
        var request = $http({
            method: "post",
            url: directorio + "api/profesores",
            data: $scope.nuevoProfesor,
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "listaProfesores";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se actualiza un profesor
    $scope.actualizarProfesor = function (id) {
        var request = $http({
            method: "post",
            url: directorio + "api/profesores/" + id,
            data: {
                txtNombre: $scope.nuevoProfesor.txtNombre,
                txtApellido: $scope.nuevoProfesor.txtApellido,
                txtDni: $scope.nuevoProfesor.txtDni,
                txtTelefono: $scope.nuevoProfesor.txtTelefono,
                _token: $scope.nuevoProfesor.token,
                selectColegio: $scope.nuevoProfesor.selectColegio,
                txtCorreo: $scope.nuevoProfesor.txtCorreo,
                txtCatedra: $scope.nuevoProfesor.txtCatedra,
                hddProfesorId: $scope.nuevoProfesor.hddProfesorId
            },
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "../listaProfesores";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se elimina un profesor
    $scope.eliminarProfesor = function (id) {
        var url = directorio + "api/profesores/" + id;
        $http.delete(url)
            .then(function (response) {
                    console.log(response);
                    if (response.data.mensaje == "OK") {
                        $('.bs-example-modal_eliminarEx').modal('show');
                        setTimeout(function () {
                            window.location.href = "listaProfesores";
                        }, 3000);
                    } else {
                        $('.bs-example-modal_error').modal('show');
                    }
                },
                function (error) {
                    $('.bs-example-modal_error').modal('show');
                });
    }
});

//CONTROLADOR PARA ALUMNOS
app.controller("alumnosController", function ($scope, $http) {
    $scope.alumnos = [];
    $scope.nuevoProfesor = {};
    $scope.colegios = [];
    //trae la lista de colegios
    $http.get(directorio + "api/colegios")
        .then(function (response) {
                console.log(response);
                $scope.colegios = response.data;
            },
            function (error) {
                console.log(error)
            });
    //trae la lista de Alumnos
    $http.get(directorio + "api/alumnos")
        .then(function (response) {
                console.log(response);
                $scope.alumnos = response.data;
            },
            function (error) {
                console.log(error)
            });
    //trae datos de  un alumno
    $scope.leerAlumno = function (id) {
        $http.get(directorio + "api/alumnos/" + id)
            .then(function (response) {
                    console.log(response);
                    $scope.nuevoAlumno.txtNombre = response.data[0].nombre;
                    $scope.nuevoAlumno.txtApellido = response.data[0].apellido;
                    $scope.nuevoAlumno.txtDni = response.data[0].dni;
                    $scope.nuevoAlumno.txtCurso = response.data[0].curso;
                    $scope.nuevoAlumno.selectColegio = response.data[0].Colegio;
                },
                function (error) {
                    console.log(error)
                });
    }
    //se registra un nuevo alumno
    $scope.agregarAlumno = function () {
        var request = $http({
            method: "post",
            url: directorio + "api/alumnos",
            data: $scope.nuevoAlumno,
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "listaAlumnos";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se actualiza un alumno
    $scope.actualizarAlumno = function (id) {
        var request = $http({
            method: "post",
            url: directorio + "api/alumnos/" + id,
            data: {
                txtNombre: $scope.nuevoAlumno.txtNombre,
                txtApellido: $scope.nuevoAlumno.txtApellido,
                txtDni: $scope.nuevoAlumno.txtDni,
                txtCurso: $scope.nuevoAlumno.txtCurso,
                _token: $scope.nuevoAlumno.token,
                selectColegio: $scope.nuevoAlumno.selectColegio,
                hddAlumnoId: $scope.nuevoAlumno.hddAlumnoId
            },
            headers: {
                'Content-Type': 'application/json, text/plain'
            }
        }).then(function (response, status, headers, config) {
                console.log(response.data);
                if (response.data.mensaje == "OK") {
                    $('.bs-example-modal').modal('show');
                    setTimeout(function () {
                        window.location.href = "../listaAlumnos";
                    }, 3000);
                } else {
                    $('.bs-example-modal_error').modal('show');
                }
            },
            function (error) {
                console.log(error)
            });
    }
    //se elimina un alumno
    $scope.eliminarAlumno = function (id) {
        var url = directorio + "api/alumnos/" + id;
        $http.delete(url)
            .then(function (response) {
                    console.log(response);
                    if (response.data.mensaje == "OK") {
                        $('.bs-example-modal_eliminarEx').modal('show');
                        setTimeout(function () {
                            window.location.href = "listaAlumnos";
                        }, 3000);
                    } else {
                        $('.bs-example-modal_error').modal('show');
                    }
                },
                function (error) {
                    $('.bs-example-modal_error').modal('show');
                });
    }
});