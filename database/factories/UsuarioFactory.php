<?php

use Faker\Generator as Faker;

$factory->define(App\Usuarios::class, function (Faker $faker) {
    static $clave;
    return [
        'nombre' =>$faker->name,
        'apellido' =>$faker->name,
        'perfil' => $faker->randomElement(['Profesor','Alumno']),
        'usuario' => $faker->userName,
        'clave' => $clave ?: $clave = bcrypt('secret')
    ];
});
