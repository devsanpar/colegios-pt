<?php

use Faker\Generator as Faker;

$factory->define(App\Alumnos::class, function (Faker $faker) {
    static $id = 21;
    return [
        'usuarioId' => $id++,
        'colegioId' =>rand(1,20),
        'curso' => $faker->text(20),
        'fechaNacimiento' => date('Y-m-d'),
        'dni' => ''
    ];
});
