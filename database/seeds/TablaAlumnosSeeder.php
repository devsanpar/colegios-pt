<?php

use Illuminate\Database\Seeder;

class TablaAlumnosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Alumnos::class,20)->create();
    }
}
