<?php

use Illuminate\Database\Seeder;

class TablaColegiosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Colegios::class,20)->create();
    }
}
