<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaAlumnos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function ($table) {
            $table->increments('alumnosId');
            $table->integer('colegioId');
            $table->integer('usuarioId');
            $table->date('fechaNacimiento');
            $table->string('curso');
            $table->string('dni')->nullable();
            $table->timestamps();

            //relaciones
            /* $table->foreign('colegioId')->references('colegioId')->on('colegios')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('usuarioId')->references('usuarioId')->on('usuarios')
                ->onDelete('cascade')
                ->onUpdate('cascade'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
