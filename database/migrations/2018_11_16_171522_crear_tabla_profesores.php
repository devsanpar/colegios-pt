<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrearTablaProfesores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesores', function ($table) {
            $table->increments('profesorId');
            $table->integer('colegioId');
            $table->integer('usuarioId');
            $table->string('telefono');
            $table->string('correo')->nullable();
            $table->string('catedra');
            $table->string('dni')->nullable();
            $table->timestamps();

            //relaciones
            /* $table->foreign('colegioId')->references('colegioId')->on('colegios')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('usuarioId')->references('usuarioId')->on('usuarios')
                ->onDelete('cascade')
                ->onUpdate('cascade'); */
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
