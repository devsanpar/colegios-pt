<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesores extends Model
{
    protected $table = 'profesores';
    protected $fillable = [];

    public function usuario(){
        return $this->belongsTo('App\Usuarios');
    }

    public function colegios(){
        return $this->hasMany('App\Colegios');
    }
}
