<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumnos extends Model
{
    protected $table = 'alumnos';
    protected $fillable = [];

    public function usuario(){
        return $this->belongsTo('App\Usuarios');
    }

    public function colegios(){
        return $this->hasMany('App\Colegios');
    }
}
