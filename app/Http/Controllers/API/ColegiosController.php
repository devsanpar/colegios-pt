<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Colegios;
use App\Profesores;
use App\Alumnos;

class ColegiosController extends Controller
{
    public function index()
    {
        try {
            $Colegios = Colegios::all();
            return $Colegios;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para buscar por id de un colegio
    public function show($colegioId)
    {
        $respuesta = "";
        try {
            $colegio = Colegios::where('colegioId', '=', $colegioId)->get();
            if ($colegio->count() == 0) {
                $respuesta = json_encode(array('colegioId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $colegio;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para dar de alta a un colegio
    public function store(Request $request)
    {
        $respuesta = "";
        try {
            $colegio = new Colegios;
            $colegio->nombre = $request->input("txtNombre");
            $colegio->direccion = $request->input("txtDireccion");
            $colegio->personaContacto = $request->input("txtPersona");
            $colegio->telefono = $request->input("txtTelefono");
            $colegio->correo = $request->input("txtCorreo");
            $colegio->web = $request->input("txtWeb");
            if ($colegio->save()) {
                $respuesta = json_encode(array('colegioId' => $colegio->id, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('colegioId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para actualizar los datos de un colegio
    public function update(Request $request)
    {
        //die($request->input('txtWeb'));
        $respuesta = "";
        try {
            $colegio = Colegios::where('colegioId', '=', $request->input('hddcolegioId'))->update([
                'nombre' => $request->input('txtNombre'),
                'direccion' => $request->input('txtDireccion'),
                'personaContacto' => $request->input('txtPersona'),
                'telefono' => $request->input('txtTelefono'),
                'correo' => $request->input('txtCorreo'),
                'web' => $request->input('txtWeb'),
            ]);
            if ($colegio) {
                $respuesta = json_encode(array('colegioId' => $request->input('hddcolegioId'), 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('colegioId' => 0, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para eliminar un colegio
    public function destroy($colegioId)
    {
        $respuesta = "";
        try {
            $colegio = Colegios::where('colegioId', '=', $colegioId)->delete();
            if ($colegio) {
                $respuesta = $respuesta = json_encode(array('colegioId' => $colegioId, 'mensaje' => 'OK'));
            } else {
                $respuesta = json_encode(array('colegioId' => $colegioId, 'mensaje' => 'ERROR'));
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );

        }
    }

    //funcion para buscar los profesores segun del colegio seleccionado
    public function getProfesoresByColegio($colegioId)
    {
        $respuesta = "";
        try {
            $profesor = Profesores::join('Usuarios', 'Profesores.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Profesores.colegioId', '=', 'Colegios.colegioId')->where('Colegios.colegioId', '=', $colegioId)->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Profesores.telefono', 'Profesores.correo', 'Profesores.catedra', 'Profesores.dni', 'Profesores.profesorId']);
            if ($profesor->count() == 0) {
                $respuesta = json_encode(array('profesorId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $profesor;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }

    //funcion para buscar los profesores segun del colegio seleccionado
    public function getAlumnosByColegio($colegioId)
    {
        $respuesta = "";
        try {
            $alumno = Alumnos::join('Usuarios', 'Alumnos.usuarioId', '=', 'Usuarios.usuarioId')->join('Colegios', 'Alumnos.colegioId', '=', 'Colegios.colegioId')->where('Colegios.colegioId', '=', $colegioId)->get(['Usuarios.*', 'Colegios.nombre as Colegio', 'Alumnos.curso', 'Alumnos.dni', 'Alumnos.fechaNacimiento', 'Alumnos.alumnosId']);
            if ($alumno->count() == 0) {
                $respuesta = json_encode(array('alumnosId' => 0, 'mensaje' => 'No existen datos para esta consulta'));
            } else {
                $respuesta = $alumno;
            }
            return $respuesta;
        } catch (Exception $e) {
            throw new QueryException(
                $query,
                $this->prepareBindings($bindings),
                $e
            );
        }
    }
}
