@extends('template/header')

@section('content')
<section class="content-header">
    <h1>
    Alumno
    </h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header">
      <div class="col-xs-2">
          @if($accion=="Crear")
            <a href="listaAlumnos" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
            @else
                <a href="../listaAlumnos" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
            @endif
        </div>
        <div class="col-xs-10">
            <div style="padding-left:25% !important" class="box-title"><h1>{{$accion}} Alumno</h1></div>
        </div>
        
      </div>

      <!-- /.box-header -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
        <div class="box-body" ng-controller="alumnosController">
        @if($accion=="Actualizar")
            <div ng-init="leerAlumno({{$alumnoId}})"></div>
        @endif
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                @if($alumnoId==0)
                    <form ng-submit="agregarAlumno()" enctype="multipart/form-data">
                @else
                    <form ng-submit="actualizarAlumno({{$alumnoId}})" enctype="multipart/form-data">
                @endif                    
                    <input type="hidden" ng-model="nuevoAlumno.token" ng-init="nuevoAlumno.token='{{{ csrf_token() }}}'" />                    
                    <input type="hidden" ng-model="nuevoAlumno.hddAlumnoId" ng-init="nuevoAlumno.hddAlumnoId='{{ $alumnoId }}'" />                    
                    
                    <!-- text input -->
                    <div class="form-group col-xs-12">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoAlumno.txtNombre" ng-init="nuevoAlumno.txtNombre=''" type="text" class="form-control" placeholder="Nombre" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Apellido</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoAlumno.txtApellido" ng-init="nuevoAlumno.txtApellido=''" type="text" class="form-control" placeholder="Apellido" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>DNI</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoAlumno.txtDni" ng-init="nuevoAlumno.txtDni=''" type="phone" class="form-control" placeholder="123456" maxlength="10" required>
                        </div>
                    </div>
                    @if($alumnoId==0)
                    <div class="form-group col-xs-12">
                        <label>Fecha de Nacimiento</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoAlumno.datefechaNac" ng-init="nuevoAlumno.datefechaNac=''" type="text" class="form-control" placeholder="1987/08/14" maxlength="10" required data-date="" data-date-format="YYYY MMMM DD">
                        </div>
                    </div>
                    @endif
                    <!-- select -->
                    <div class="form-group col-xs-12">
                        <label>Colegio</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-language"></i></span>
                            <select class="form-control" ng-model="nuevoAlumno.selectColegio">
                                <option ng-repeat="cole in colegios" value="[[cole.colegioId]]" ng-init="nuevoAlumno.selectColegio=''">[[cole.nombre]]</option>
                                
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Curso</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoAlumno.txtCurso" ng-init="nuevoAlumno.txtCurso=''" type="text" class="form-control" placeholder="primer grado" maxlength="100" required>
                        </div>
                    </div>

                    
                    <div class="col-xs-5"></div>
                    <div class="col-xs-2">
                        <button type="submit" class="btn btn-block btn-success btn-flat"><i class="fa fa-floppy-o"></i> Guardar</button>
                    </div>
                    <div class="col-xs-5"></div>
                </form>
            </div>
            <div class="col-xs-2"></div>
        </div>
      
    <!-- /.box -->

    

  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scriptspagina')
    <!-- <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script type="text/javascript">
    var msj= $("#mensaje").val();
    if(msj==1){
        $('.bs-example-modal').modal('show');
    }
    else if(msj==2){
        $('.bs-example-modal_error').modal('show');
    }
    else if(msj==3){
        $('.bs-example-modal_duplicado').modal('show');
    }

    $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1', {
        allowedContent: true,
    });
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });

  $(document).ready(function(){
        $("#selectIdioma").change(function(){
            $("#selectCategorias").removeAttr("disabled");
            var id= $("#selectIdioma").val();
            var catPadre= $("#selectCatPadre").val();
            $.get("ajaxCatCalidadIdioma="+id, function(data) {
                var json= data;
                var datos= JSON.parse(json);
                //elimino las opciones actuales
                $('#selectCategorias option').remove();
                //Creo las nuevas opciones segun el idioma
                for(x=0; x<datos.length; x++) {
                    $('#selectCategorias').append($('<option>', {
                        value: datos[x].idCatCalidad,
                        text: datos[x].nombreCategoria
                    }));
                }
            });
            $("#btnGuardar").removeAttr("disabled","false");
        });//fin change

    })
</script> -->

@stop