@extends('template/header')

@section('content')
    <section class="content-header">
        <h1>
            Profesores
        </h1>
    </section>
    <section class="content" ng-controller="profesoresController">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="col-xs-10">
                            <div style="text-align:center !important" class="box-title">Listado de Profesores</div>
                        </div>
                        <div class="col-xs-2">
                            <a href="agregarProfesor" class="btn btn-block btn-primary btn-flat"><i
                                        class="fa fa-edit"></i>Crear </a>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->

                    <div class="box-body">
                        <table id="example" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>DNI</th>
                                <th>Catedra</th>
                                <th>Correo</th>
                                <th>Telefono</th>                                
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="item in profesores">                                    
                                    <td>[[item.nombre]]</td>
                                    <td>[[item.apellido]]</td>
                                    <td>[[item.dni]]</td>
                                    <td>[[item.catedra]]</td>
                                    <td>[[item.correo]]</td>
                                    <td>[[item.telefono]]</td>
                                    <td>
                                    @php $profesorId = "[[item.profesorId]]"; @endphp  
                                    <a style="margin-left: 20px" href="agregarProfesor/[[item.profesorId]]"><i class="fa fa-pencil"></i></a> &nbsp;&nbsp;
                                    <a style="margin-left: 20px" href="#" ng-click="eliminarProfesor([[item.profesorId]])"><i class="fa fa-trash"></i></a>
                                    </td>                                    
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
@section('scriptspagina')
    <script type="text/javascript">
        function borrar(id) {
            var r = confirm("Eliminar categoria?");
            if (r == true) {
                $.ajax({
                    url: "deleteCalidad/" + id,
                    type: "get"
                })
                    .done(function (res) {
                        //alert(res)
                        if (res == "OK") {
                            $('.bs-example-modal').modal('show');
                            window.location.href = "getCalidad";
                        }
                        else if (res == "ERROR") {
                            $('.bs-example-modal_error').modal('show');
                        }
                        else {
                            $('.bs-example-modal_duplicado').modal('show');
                        }
                    });
            }

        }
    </script>

@stop