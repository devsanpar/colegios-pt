@extends('template/header')

@section('content')
<section class="content-header">
    <h1>
    Profesor
    </h1>
</section>
<section class="content">
<div class="row">
  <div class="col-xs-12">
    <div class="box box-primary">
      <div class="box-header">
      <div class="col-xs-2">
          @if($accion=="Crear")
            <a href="listaProfesores" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
            @else
                <a href="../listaProfesores" class="btn btn-block btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Regresar</a>
            @endif
        </div>
        <div class="col-xs-10">
            <div style="padding-left:25% !important" class="box-title"><h1>{{$accion}} Profesor</h1></div>
        </div>
        
      </div>

      <!-- /.box-header -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
        <div class="box-body" ng-controller="profesoresController">
        @if($accion=="Actualizar")
            <div ng-init="leerProfesor({{$profesorId}})"></div>
        @endif
            <div class="col-xs-2"></div>
            <div class="col-xs-8">
                @if($profesorId==0)
                    <form ng-submit="agregarProfesor()" enctype="multipart/form-data">
                @else
                    <form ng-submit="actualizarProfesor({{$profesorId}})" enctype="multipart/form-data">
                @endif                    
                    <input type="hidden" ng-model="nuevoProfesor.token" ng-init="nuevoProfesor.token='{{{ csrf_token() }}}'" />                    
                    <input type="hidden" ng-model="nuevoProfesor.hddProfesorId" ng-init="nuevoProfesor.hddProfesorId='{{ $profesorId }}'" />
                    <!-- text input -->
                    <div class="form-group col-xs-12">
                        <label>Nombre</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtNombre" ng-init="nuevoProfesor.txtNombre=''" type="text" class="form-control" placeholder="Nombre" maxlength="100" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Apellido</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtApellido" ng-init="nuevoProfesor.txtApellido=''" type="text" class="form-control" placeholder="Apellido" maxlength="100" required>
                        </div>
                    </div>
                    <div class="form-group col-xs-12">
                        <label>DNI</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtDni" ng-init="nuevoProfesor.txtDni=''" type="phone" class="form-control" placeholder="123456" maxlength="10" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Telefono</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtTelefono" ng-init="nuevoProfesor.txtTelefono=''" type="phone" class="form-control" placeholder="23456789" maxlength="10" required>
                        </div>
                    </div>

                    <div class="form-group col-xs-12">
                        <label>Correo</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtCorreo" ng-init="nuevoProfesor.txtCorreo=''" type="email" class="form-control" placeholder="profesor@colegio.com" maxlength="100" required>
                        </div>
                    </div>

                    <!-- select -->
                    <div class="form-group col-xs-12">
                        <label>Colegio</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-language"></i></span>
                            <select class="form-control" ng-model="nuevoProfesor.selectColegio">
                                <option ng-repeat="cole in colegios" value="[[cole.colegioId]]" ng-init="nuevoProfesor.selectColegio=''">[[cole.nombre]]</option>
                                
                            </select>
                        </div>
                    </div> 

                    <div class="form-group col-xs-12">
                        <label>Catedra</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bars"></i></span>
                            <input ng-model="nuevoProfesor.txtCatedra" ng-init="nuevoProfesor.txtCatedra=''" type="text" class="form-control" placeholder="matematicas" maxlength="100" required>
                        </div>
                    </div>

                   
                    <div class="col-xs-5"></div>
                    <div class="col-xs-2">
                        <button type="submit" class="btn btn-block btn-success btn-flat"><i class="fa fa-floppy-o"></i> Guardar</button>
                    </div>
                    <div class="col-xs-5"></div>
                    <!-- <input name="mensaje" id="mensaje" type="hidden" value="{$idmsj}}"> -->
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                </form>
            </div>
            <div class="col-xs-2"></div>
        </div>
      
    <!-- /.box -->

    

  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
@endsection

@section('scriptspagina')
    <!-- <script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script type="text/javascript">
    var msj= $("#mensaje").val();
    if(msj==1){
        $('.bs-example-modal').modal('show');
    }
    else if(msj==2){
        $('.bs-example-modal_error').modal('show');
    }
    else if(msj==3){
        $('.bs-example-modal_duplicado').modal('show');
    }

    $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1', {
        allowedContent: true,
    });
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();
  });

  $(document).ready(function(){
        $("#selectIdioma").change(function(){
            $("#selectCategorias").removeAttr("disabled");
            var id= $("#selectIdioma").val();
            var catPadre= $("#selectCatPadre").val();
            $.get("ajaxCatCalidadIdioma="+id, function(data) {
                var json= data;
                var datos= JSON.parse(json);
                //elimino las opciones actuales
                $('#selectCategorias option').remove();
                //Creo las nuevas opciones segun el idioma
                for(x=0; x<datos.length; x++) {
                    $('#selectCategorias').append($('<option>', {
                        value: datos[x].idCatCalidad,
                        text: datos[x].nombreCategoria
                    }));
                }
            });
            $("#btnGuardar").removeAttr("disabled","false");
        });//fin change

    })
</script> -->

@stop