<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrador de Colegios</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!--<link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">-->
    {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <!--<link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">-->
  {!! Html::style('/plugins/datatables/dataTables.bootstrap.css') !!}
  <!-- Theme style -->
  <!--<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">-->
  {!! Html::style('/dist/css/AdminLTE.min.css') !!}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <!--<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">-->
  {!! Html::style('/dist/css/skins/_all-skins.min.css') !!}
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  {!! Html::style('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
  {!! Html::style('/css/flag-icon-css-master/css/flag-icon.min.css') !!}
  
</head>
<body class="hold-transition login-page">

@yield('content')

<!-- jQuery 2.2.0 -->
{!! Html::script('/plugins/jQuery/jQuery-2.2.0.min.js') !!}
<!--<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>-->
<!-- Bootstrap 3.3.6 -->
{!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
<!--<script src="../../bootstrap/js/bootstrap.min.js"></script>-->
<!-- DataTables -->
{!! Html::script('/plugins/datatables/jquery.dataTables.min.js') !!}
{!! Html::script('/plugins/datatables/dataTables.bootstrap.min.js') !!}
<!--<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>-->
<!-- SlimScroll -->
{!! Html::script('/plugins/slimScroll/jquery.slimscroll.min.js') !!}
<!--<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>-->
<!-- FastClick -->
{!! Html::script('/plugins/fastclick/fastclick.js') !!}
<!--<script src="../../plugins/fastclick/fastclick.js"></script>-->
<!-- AdminLTE App -->
{!! Html::script('/dist/js/app.min.js') !!}
<!--<script src="../../dist/js/app.min.js"></script>-->
<!-- AdminLTE for demo purposes -->
{!! Html::script('/dist/js/demo.js') !!}
<!--<script src="../../dist/js/demo.js"></script>-->
<!-- page script -->
{!! Html::script('/js/scriptsPagina.js') !!}
{!! Html::script('https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js') !!}
{!! Html::script('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') !!}
{!! Html::script('/front/js/vue.js') !!}



<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });
  });
  
</script>

@yield('scriptspagina')
</body>
</html>
