<!DOCTYPE html>
<html ng-app="pruebaTecnica">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrador de Colegios</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <!--<link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">-->
    {!! Html::style('/bootstrap/css/bootstrap.min.css') !!}
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <!--<link rel="stylesheet" href="../../plugins/datatables/dataTables.bootstrap.css">-->
  {!! Html::style('/plugins/datatables/dataTables.bootstrap.css') !!}
  <!-- Theme style -->
  <!--<link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">-->
  {!! Html::style('/dist/css/AdminLTE.min.css') !!}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <!--<link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">-->
  {!! Html::style('/dist/css/skins/_all-skins.min.css') !!}
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  {!! Html::style('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') !!}
  {!! Html::style('/css/flag-icon-css-master/css/flag-icon.min.css') !!}
  <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.5/angular.min.js"></script>
  {!! Html::script('/ControladoresAngular/GeneralController.js') !!}
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- MENSAJES MODALES -->
<div class="modal fade bs-example-modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-title modal-success">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa  fa-comment"></i> Registro Exitoso!!!!</h4>
        </div>
        <div class="modal-body">
          <p>Los Datos se han registrado exitosamente.</p>
          <p style=" text-align: center; " ><i class="fa fa-check-square"></i></p>
        </div>
      </div>
  </div>
</div>

<div class="modal fade bs-example-modal_error" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-title modal-danger">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa  fa-comment"></i> ERROR!!!!</h4>
        </div>
        <div class="modal-body">
          <p>Los Datos NO se han podido registrar. Por favor, verifique que los datos esten correcto.</p>
          <p style=" text-align: center; " ><i class="fa fa-times-circle"></i></p>
        </div>
      </div>
  </div>
</div>

<div class="modal fade bs-example-modal_duplicado" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-title modal-warning">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa  fa-comment"></i> Precaución!!!!</h4>
        </div>
        <div class="modal-body">
          <p>Los Datos datos que intenta registrar ya están almacenados. Por favor, verifique que los datos esten correcto.</p>
          <p style=" text-align: center; " ><i class="fa  fa-warning"></i></p>
        </div>
      </div>
  </div>
</div>

<div class="modal fade bs-example-modal_eliminarEx" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm modal-title modal-success">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa  fa-comment"></i> Eliminacion Exitosa!!!!</h4>
        </div>
        <div class="modal-body">
          <p>Se ha borrado el registro con exito.</p>
          <p style=" text-align: center; " ><i class="fa fa-check-square"></i></p>
        </div>
      </div>
  </div>
</div>




<!-- /MENSAJES MODALES -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{url('/')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>I</b>Admin</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Colegios</b>Admin</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Panel de navegacion</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{URL::asset('dist/img/avatar5.png')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">Jose Humberto Parisi</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{URL::asset('dist/img/avatar5.png')}}" class="img-circle" alt="User Image">

                <p>
                jhparisi <br> email
                </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                <form action="logout" method="post">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                <button type="submit" class="btn btn-default btn-flat">Cerrar Sesión</button>
                </form>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{URL::asset('dist/img/avatar5.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><b>usuario</b></p>
          <input type="hidden" name="perfil" id="perfil" value="">
          <!--va aqui-->
          <small><span id="idPerfil"></span></small>
        </div>
      </div>
      
      <!-- sidebar menu: : style can be found in sidebar.less -->
      
      <ul class="sidebar-menu">
        <li class="header">MENÚ PRINCIPAL</li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-window-maximize"></i> <span>Colegios</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ url('listaColegios') }}"><i class="fa fa-circle-o"></i> Lista de colegios</a></li>
            <li><a href="{{ url('listaProfesores') }}"><i class="fa fa-circle-o"></i> Crear Profesor</a></li>
            <li><a href="{{ url('listaAlumnos') }}"><i class="fa fa-circle-o"></i> Crear Alumnos</a></li>
          </ul>
        </li>
        
        <li>
          <a href="{{ url('listaUsuarios') }}">
            <i class="fa fa-user"></i> <span>Crear Usuarios Admin</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

<!--JHPC-->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <!--AQUI VA EL CONTENIDO DE CADA PAGINA-->
    @yield('content')
    
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Prueba Tecnica:</strong> Jose Humberto Parisi.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.0 -->
{!! Html::script('/plugins/jQuery/jQuery-2.2.0.min.js') !!}
<!--<script src="../../plugins/jQuery/jQuery-2.2.0.min.js"></script>-->
<!-- Bootstrap 3.3.6 -->
{!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
<!--<script src="../../bootstrap/js/bootstrap.min.js"></script>-->
<!-- DataTables -->
{!! Html::script('/plugins/datatables/jquery.dataTables.js') !!}
{!! Html::script('/plugins/datatables/dataTables.bootstrap.min.js') !!}
<!--<script src="../../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../../plugins/datatables/dataTables.bootstrap.min.js"></script>-->
<!-- SlimScroll -->
{!! Html::script('/plugins/slimScroll/jquery.slimscroll.min.js') !!}
<!--<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>-->
<!-- FastClick -->
{!! Html::script('/plugins/fastclick/fastclick.js') !!}
<!--<script src="../../plugins/fastclick/fastclick.js"></script>-->
<!-- AdminLTE App -->
{!! Html::script('/dist/js/app.min.js') !!}
<!--<script src="../../dist/js/app.min.js"></script>-->
<!-- AdminLTE for demo purposes -->
{!! Html::script('/dist/js/demo.js') !!}
<!--<script src="../../dist/js/demo.js"></script>-->
<!-- page script -->
{!! Html::script('/js/scriptsPagina.js') !!}
{{--<script src="//cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>--}}


<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": false,
      "info": false,
      "autoWidth": true
    });
  });

  $(function () {

  });

  
</script>

@yield('scriptspagina')
</body>
</html>
